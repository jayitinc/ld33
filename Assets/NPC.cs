﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour
{
    public Faction faction = Faction.Civilian;

    private World world;

    void Start()
    {
        world = GameObject.Find("World").GetComponent<World>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Game.COINS += Game.ROUND;
        world.npcs.Remove(gameObject);
        Destroy(gameObject);
    }
}

public enum Faction
{
    Civilian, Police
}