﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    public List<GameObject> npcs = new List<GameObject>();

    public GameObject[] civilians = { };

    void Start()
    {
        for (int i = 0; i < 25; i++)
        {
            float spawnX = Random.Range(-15.5f, 15.5f);
            float spawnY = Random.Range(-8.5f, 8.5f);

            int randomCivilian = Random.Range(0, civilians.Length - 1);

            GameObject go = Instantiate(civilians[randomCivilian], new Vector2(spawnX, spawnY), Quaternion.identity) as GameObject;
            go.name = "Civilian";
            npcs.Add(go);
        }
    }

    void Update()
    {
        if (npcs.Count < 1)
            Game.ROUND++;
    }
}